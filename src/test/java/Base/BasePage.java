package Base;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.Managed;
import org.apache.tools.ant.taskdefs.WaitFor;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static java.time.temporal.ChronoUnit.SECONDS;
import static org.junit.Assert.assertTrue;

import java.util.logging.Logger;


public class BasePage extends PageObject {

    protected static int defaultTimeout = 40;

    @Managed
    private static WebDriver driver;

    public static Logger logger = Logger.getLogger("Candidate 2060354");

    public void WaitForWebElement(WebElementFacade element)
     {
        try{
            WebDriverWait wait = new WebDriverWait(driver, 1);
            wait.until(ExpectedConditions.visibilityOf(element));
            wait.until(ExpectedConditions.elementToBeClickable(element));
        } catch (Exception e) {
            Log("Exception occurred while Waiting for Element, Exception :" + e.getMessage());
        }
     }

     public static void Log(String text)
     {
         logger.info(text);

     }

     public void click (WebElementFacade element)
     {
         WaitForWebElement(element);
         element.click();
     }

     public  void enterKeys(WebElementFacade element, String text)
     {
         WaitForWebElement(element);
         element.sendKeys(text);

     }

     public  void getText(WebElementFacade element)
     {
         WaitForWebElement(element);
         element.getText();
     }

     public void mouseHover(WebElementFacade element)
     {
         WaitForWebElement(element);
         withAction().moveToElement(element).perform();
     }

     public void navigate( String url)
     {
         Log("Navigating to URL: " + url);
         driver.get(url);
         WaitForPageRefresh();
     }

    protected  void WaitForPageRefresh() {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        for (int i = 0; i < defaultTimeout; i++) {
            if (js.executeScript("return document.readyState").toString().equals("complete")) {
                break;
            }
            setImplicitTimeout(5, SECONDS);
        }
        setImplicitTimeout(5, SECONDS);
    }

    public void selectbyVisibleText(WebElementFacade element, String text)
    {
        Log("Selecting " + text + " from " + element + " dropdown");
        WaitForWebElement(element);
        element.selectByVisibleText(text);
    }
    public void selectbyValue(WebElementFacade element, String value)
    {
        WaitForWebElement(element);
        element.selectByValue(value);
    }

    public void AssertResult(WebElementFacade element, String expected)
    {
        String text =element.getText();
        assertTrue("Response matches expected: '" + expected + "'" + " Actual: '" + text , text.equals(expected));

    }


}
